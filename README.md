# My Notes To You - By Oren
Hi.  
These are a few notes that i thought would be important to know before running and viewing the app.

1. I understood that no 3rd party is allowed - so - i created my own ngrx/store (Redux) implementation in AppState 
2. I used bootstrap-sass as a framework 
3. I wrote specs to some of the components - sometimes choosing different techniques
4. Responsive: I tested the app for Nexus5x, Nexus7, iPhone6, MDPI, HiDPI resolutions. It works on other resolutions as well, but i didn't tested it on all.
5. **important**: I missed the last sentence in this readme - "Getting Started" - so - i started with creating a bar-charts component. However, I chose to include it in this app as it interacts with the app's sidebar for replacing the data with animation. 

Thanks,  
Oren Farhi  
[orizens.com](http://orizens.com)

# Infomedia code test

This test is designed to asses your skill level as a front end developer, your ability to follow a brief and work autonomously.

Read this guide carefully as you may miss something if you don't.

We have supplied one of the most common and complete starters to save you time on config.

## We are looking at the following skills( not in any specific order):

- Responsive design
- Sass / css
- Semantics
- Accessibility
- Angular2
- Automated testing
- Git
- Redux
- Attention to detail
- Problem solving ability

## The brief

Create a dashboard with 3 charts on it, the charts must be animated, and the dashboard, must be responsive. Once you're done create a github repo and send a link the contact you spoke to at Infomedia.

## The rules

1. You must bind the charts via a service
2. You must include unit tests
3. You must use typescript
4. You must comment your code
5. You will need to demonstrate knowledge of Sass by adding a sass based framework into the test.
6. You must lint you typescript.

## Bonuses - these will be in your favour

- Performance, if you make the code performant by adding any special techniques (hint there are examples of techniques in this starter)
- The more re-usable your code is, the better extra points for thinking outside the box
- If the animated chart uses css3 rather than just javascript this will be
- Observables, we love em' you should too
- Surprise us, show us that you're passionate about your work, and the quality of it

## The end result should be:

Simple, testable & well formatted.

## Stuff you can leave out

- minification & concatenation
- cross browser support
- end to end testing (although, this project has it so you can use it).

## javascript

- DO NOT USE ANY OTHER LIBRARIES - use only what's included in the dependencies.

## Pre requisites

- node
- npm

## Getting started

1. Screen creative for desktop supplied under the creative folder.

