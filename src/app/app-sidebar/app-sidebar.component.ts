import { AppState } from '../app.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, HostBinding, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as ChartsActions from '../store/charts.reducer';

const AxisProps = ChartsActions.ChartProps.Axis;

@Component({
  selector: 'app-sidebar',
  styleUrls: ['./app-sidebar.scss'],
  template: `
    <div class="list-group">
      <a routerLink="{{ item.url }}"
        routerLinkActive="active"
        [routerLinkActiveOptions]="{ exact: true }"
        *ngFor="let item of listItems" class="list-group-item">
        <i class="{{ iconPrefix }} {{iconPrefix}}-{{ item.icon }}"></i> {{ item.label }}
      </a>
    </div>
    <form [formGroup]="propsForm" novalidate class="series list-group">
      <div class="radio list-group-item" *ngFor="let prop of props; let index = index">
        <label for="{{prop.value}}-{{index}}">
          <input type="radio" id="{{prop.value}}-{{index}}"
            formControlName="prop"
            [checked]="isChecked(prop, index)"
            [value]="prop.value">
          {{ prop.label }}
        </label>
      </div>
    </form>
  `
})
export class AppSidebarComponent implements OnInit {

  public iconPrefix = 'glyphicon';

  public listItems = [
    { icon: 'dashboard', label: 'Main', url: '/' },
    { icon: 'user', label: 'About', url: '/about' }
  ];

  public props = [
    { label: 'Profits', value: AxisProps.PROFITS },
    { label: 'Inventory', value: AxisProps.INVENTORY }
  ];

  public propsForm: FormGroup;
  public localState: any;

  private checkedIndex = 0;

  constructor(
    public route: ActivatedRoute,
    public appState: AppState,
    private fb: FormBuilder,
  ) {
    this.createForm();
  }

  public ngOnInit() {
    this.route
      .data
      .subscribe((data: any) => {
        // your resolved data from route
        this.localState = data.yourData;
      });
    this.propsForm.valueChanges.subscribe((propForm) => {
      this.appState.dispatch(new ChartsActions.UpdateAxisPropToShow(propForm.prop));
    });
  }

  public createForm() {
    this.propsForm = this.fb.group({
      prop: ''
    });
  }

  public isChecked(prop, index: number) {
    return this.checkedIndex === index;
  }
}
