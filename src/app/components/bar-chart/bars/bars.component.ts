import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit } from '@angular/core';
import { IChartSet, IChartItem } from '../../../models';
@Component({
  selector: 'bars',
  styleUrls: [ './bars.component.scss' ],
  template: `
  <div *ngFor="let bar of bars" class="bar-item" 
    [class.animate]="hasWidth(bar)"
    [ngStyle]="{'width.%': toPercent(bar)}">
  	<div class="bar-item__vis"><aside>{{ toPercent(bar) }}%</aside></div>
	</div>
	`,
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class BarsComponent implements OnChanges {
  @Input() public bars: IChartSet = [];
  @Input() public valueProp = '';

  private maxValue: number = 0;

  public ngOnChanges() {
    this.maxValue = this.findMaxValueForProp(this.bars, this.valueProp);
  }

  public toPercent(bar: IChartItem) {
    const value = bar[this.valueProp];
    const width = Math.round(value / this.maxValue * 100);
    return width;
  }

  public hasWidth(bar: IChartItem) {
    const hasWidth = bar[this.valueProp] !== undefined;
    return hasWidth;
  }

  private findMaxValueForProp(data: IChartSet = [], prop: string) {
    const max = Math.max.apply(null, data.map((item) => item[prop]));
    return max;
  }
}
