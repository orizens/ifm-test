import { TestBed, inject } from '@angular/core/testing';

import { BarsComponent } from './bars.component';

describe('a bars component', () => {
  let component: BarsComponent;

  // register all needed dependencies
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BarsComponent
      ]
    });
  });

	// instantiation through framework injection
  beforeEach(inject([BarsComponent], (BarsComponent) => {
    component = BarsComponent;
  }));

  it('should have an instance', () => {
    expect(component).toBeDefined();
  });
});
