import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'yaxis',
  styleUrls: [ './yaxis.component.scss' ],
  template: `
  <div *ngFor="let label of labels" class="yaxis-item">
    {{ label }}
  </div>
	`
})

export class YaxisComponent {
  @Input() public labels = [];
}
