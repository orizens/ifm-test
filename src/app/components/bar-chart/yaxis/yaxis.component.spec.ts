import { TestBed, inject, ComponentFixture } from '@angular/core/testing';

import { YaxisComponent } from './yaxis.component';

describe('a yaxis component', () => {
  let component: YaxisComponent;
  let fixture: ComponentFixture<YaxisComponent>;

  // register all needed dependencies
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ YaxisComponent ],
      providers: []
    });
  });

  // instantiation through framework injection
  beforeEach(() => {
    fixture = TestBed.createComponent(YaxisComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should have an instance', () => {
    expect(component).toBeDefined();
  });

  it('should have no labels by default', () => {
    const actual = component.labels.length;
    const expected = 0;
    expect(actual).toBe(expected);
  });

  it('should render labels', () => {
    component.labels = [ 'jan', 'feb'];
    fixture.detectChanges();
    const actual = fixture.nativeElement.querySelectorAll('.yaxis-item').length;
    const expected = component.labels.length;
    expect(actual).toEqual(expected);
  });
});
