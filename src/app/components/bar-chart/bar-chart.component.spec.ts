import { TestBed, inject } from '@angular/core/testing';

import { BarChartComponent } from './bar-chart.component';

describe('a bar-chart component', () => {
  let component: BarChartComponent;

  // register all needed dependencies
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BarChartComponent
      ]
    });
  });

  // instantiation through framework injection
  beforeEach(inject([BarChartComponent], (BarChartComponent) => {
    component = BarChartComponent;
  }));

  it('should have an instance', () => {
    expect(component).toBeDefined();
  });
});
