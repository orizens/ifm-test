import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'bar-chart',
  styleUrls: [ './bar-chart.component.scss' ],
  template: `
		<section class="container-fluid">
      <yaxis class="col-md-3 col-xs-3"
        [labels]="yaxis">
      </yaxis>
			<bars class="col-md-9 col-xs-9"
				[bars]="data"
				[valueProp]="axisProp"
			></bars>
		</section>
	`,
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class BarChartComponent {
  @Input() public axisProp = '';
  @Input() public yaxisProp = '';
  @Input() public data = [];

  get yaxis() {
    const prop = this.yaxisProp;
    return this.data.map((item) => item[prop]);
  }
}
