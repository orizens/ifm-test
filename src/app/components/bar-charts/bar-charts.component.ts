import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { IChartsState } from '../../models';

@Component({
  selector: 'bar-charts',
  styleUrls: [ './bar-charts.scss' ],
  template: `
  <bar-chart class="col-md-4"
    *ngFor="let chartData of data.sets"
		[data]="chartData"
    [axisProp]="data.axisProp"
    [yaxisProp]="data.yaxisProp">
	</bar-chart>
	`,
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class BarChartsComponent {
  @Input() public data: IChartsState;
}
