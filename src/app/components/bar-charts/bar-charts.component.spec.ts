import { TestBed, inject } from '@angular/core/testing';

import { BarChartsComponent } from './bar-charts.component';

describe('a bar-charts component', () => {
  let component: BarChartsComponent;

  // register all needed dependencies
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        BarChartsComponent
      ]
    });
  });

  // instantiation through framework injection
  beforeEach(inject([BarChartsComponent], (BarChartsComponent) => {
    component = BarChartsComponent;
  }));

  it('should have an instance', () => {
    expect(component).toBeDefined();
  });
});
