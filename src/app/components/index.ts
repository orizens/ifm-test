import { BarChartComponent, BarsComponent, YaxisComponent } from './bar-chart';
import { BarChartsComponent } from './bar-charts';
import { DonutChartComponent } from './donut-chart';
import { DonutChartsComponent } from './donut-charts';

export let CORE_COMPONENTS = [
  BarChartComponent,
  BarsComponent,
  YaxisComponent,
  BarChartsComponent,
  DonutChartComponent,
  DonutChartsComponent
];
