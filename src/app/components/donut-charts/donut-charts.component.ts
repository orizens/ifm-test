import { IStatsChartsState } from '../../models';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'donut-charts',
  styleUrls: [ './donut-charts.component.scss' ],
  template: `
  <donut-chart class="col-md-4 col-xs-12"
    *ngFor="let chartData of data.sets"
    [average]="chartData.average"
    [label]="chartData.label"
    [total]="chartData.total"
    [value]="chartData.value"
    [color]="chartData.color"
  ></donut-chart>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DonutChartsComponent {
  @Input() public data: IStatsChartsState;
}
