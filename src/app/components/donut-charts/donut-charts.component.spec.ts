import { TestBed, inject } from '@angular/core/testing';

import { DonutChartsComponent } from './donut-charts.component';

describe('a donut-charts component', () => {
  let component: DonutChartsComponent;

  // register all needed dependencies
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DonutChartsComponent
      ]
    });
  });

  // instantiation through framework injection
  beforeEach(inject([DonutChartsComponent], (DonutChartsComponent) => {
    component = DonutChartsComponent;
  }));

  it('should have an instance', () => {
    expect(component).toBeDefined();
  });
});
