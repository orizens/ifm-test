import { TestBed, inject } from '@angular/core/testing';

import { DonutChartComponent } from './donut-chart.component';

describe('a donut-chart component', () => {
  let component: DonutChartComponent;

  // register all needed dependencies
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DonutChartComponent
      ]
    });
  });

  // instantiation through framework injection
  beforeEach(inject([DonutChartComponent], (DonutChartComponent) => {
    component = DonutChartComponent;
  }));

  it('should have an instance', () => {
    expect(component).toBeDefined();
  });
});
