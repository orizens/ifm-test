import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'donut-chart',
  styleUrls: [ './donut-chart.component.scss' ],
  template: `
  <ul class="chart-skills" [style.color]="color">
    <li
      [style.transform]="getRotationDeg()"
      [style.border-color]="color"
      >
      <span></span>
    </li>
  </ul>
  <aside class="chart__value">{{ value }}%</aside>
  <aside class="chart__label">{{ label }}</aside>
  
  <section class="chart__details">
    <div class="chart__value-portion">{{ value }} / 100</div>
    <div class="chart__value-total">
      <label>Total</label>
      <span>{{ currency }}{{ total }}</span>
    </div>
    <div class="chart__value-avg">
      <label>Avg</label>
      <span>{{ currency }}{{ average }}</span>
    </div>
  </section>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class DonutChartComponent {
  @Input() public label = '';
  @Input() public value = 0;
  @Input() public total = '';
  @Input() public average = '';
  @Input() public currency = '$';
  @Input() public color = '';

  // calculates the rotation degree by the value
  // for filling the donut
  public getRotationDeg() {
    const totalDeg = 180;
    const rotationDeg = Math.round(this.value / 100 * totalDeg);
    return `rotate(${rotationDeg}deg)`;
  }
}
