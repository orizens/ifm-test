import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  inject,
  async,
  TestBed,
  ComponentFixture
} from '@angular/core/testing';

// Load the implementations that should be tested
import { AppComponent } from './app.component';
import { AppState } from './app.service';
import { ProfitsCenterService } from './services/profits-center.service';
import { StatsCenterService } from './services/stats-center.service';

describe(`App`, () => {
  let comp: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  const ProfitsCenterStub = {
    getProfits: () => jasmine.createSpyObj('getProfits', ['subscribe'])
  };
  const StatsCenterStub = {
    getStats: () => jasmine.createSpyObj('getStats', ['subscribe'])
  };

  // async beforeEach
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [AppState,
        { provide: ProfitsCenterService, useValue: ProfitsCenterStub },
        { provide: StatsCenterService, useValue: StatsCenterStub }
      ]
    })
    .compileComponents(); // compile template and css
  }));

  // synchronous beforeEach
  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    comp    = fixture.componentInstance;

    fixture.detectChanges(); // trigger initial data binding
  });

  it(`should be readly initialized`, () => {
    expect(fixture).toBeDefined();
    expect(comp).toBeDefined();
  });

  it('should call load on init', () => {
    spyOn(comp, 'load');
    comp.ngOnInit();
    expect(comp.load).toHaveBeenCalled();
  });

  it('should have a menuOpen observable member', () => {
    const actual = comp.menuOpen$;
    expect(actual).toBeDefined();
  });
});
