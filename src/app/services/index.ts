import { ProfitsCenterService } from './profits-center.service';
import { StatsCenterService } from './stats-center.service';

export const APP_SERVICES = [
  ProfitsCenterService,
  StatsCenterService
];
