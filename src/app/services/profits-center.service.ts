import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ProfitsCenterService {
  private url = 'assets/mock-data/mock-chart-data.json';
  constructor(private http: Http) { }

  public getProfits () {
    return this.http.get(this.url)
      .map((response: Response) => response.json());
  }
}
