import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { IStatsChartItem } from '../models';
@Injectable()
export class StatsCenterService {
  private url = 'assets/mock-data/mock-donut-data.json';
  constructor(private http: Http) { }

  public getStats () {
    return this.http.get(this.url)
      .map((response: Response) => response.json());
  }

  public getRandomStats () {
    return this.getStats()
      .map((stats: IStatsChartItem[]) => {
        return stats.map(this.generateItem);
      });
  }

  private generateItem (statItem: IStatsChartItem) {
    const generateNum = (min, max) => Math.floor(Math.random() * max) + min;
    statItem.value = generateNum(1, 95);
    statItem.average = `${generateNum(100, 700)}k`;
    statItem.total = `${generateNum(100, 700)}m`;
    statItem.color = [
      `rgb(${generateNum(30, 225)}`,
      `${generateNum(100, 155)}`,
      `${generateNum(20, 235)})`
    ].join(',');
    return statItem;
  }
}
