export interface IUserProfile {
  name: string;
  image?: string;
  website?: string;
};

export interface IChartItem {
  month: string;
  profits: number;
  inventory: number;
}

export interface IAppSettings {
  menuOpen: boolean;
  appName: string;
}
export type IChartSet = IChartItem[];

export interface IChartsState {
  sets: IChartSet[];
  axisProp: string;
  yaxisProp: string;
}

export interface IStatsChartItem {
  value: number;
  label: string;
  total: string;
  average: string;
  color: string;
}
export type IStatsChartSet = IStatsChartItem[];

export interface IStatsChartsState {
  sets: IStatsChartSet;
}

export interface IAppStore {
  appName?: string;
  author?: IUserProfile;
  charts?: IChartsState;
  app?: IAppSettings;
  stats?: IStatsChartsState;
}
