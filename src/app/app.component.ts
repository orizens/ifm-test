import { StatsCenterService } from './services/stats-center.service';
import { IAppStore } from './models';
import * as ChartsActions from './store/charts.reducer';
import * as StatsActions from './store/stats.reducer';
/*
 * Angular 2 decorators and services
 */
import {
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { AppState } from './app.service';
import { ProfitsCenterService } from './services/profits-center.service';

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    './app.component.scss'
  ],
  template: `
    <app-topbar></app-topbar>
    <app-sidebar class="col-md-3 col-xs-12 navbar navbar-inverse"
      [class.sidebar-collapse]="menuOpen$ | async"
      ></app-sidebar>
    <main class="col-md-9 col-md-offset-3 col-xs-12">
      <router-outlet></router-outlet>
    </main>
  `
})
export class AppComponent implements OnInit {
  public menuOpen$ = this.appState.select((store: IAppStore) => store.app.menuOpen);
  constructor(
    public appState: AppState,
    public profitsCenter: ProfitsCenterService,
    public statsCenter: StatsCenterService
  ) {}

  public ngOnInit() {
    this.load();
  }

  public load() {
    this.profitsCenter.getProfits().subscribe((response) => {
      this.appState.dispatch(new ChartsActions.AddCharts(response));
    });
    this.statsCenter.getStats().subscribe((response) => {
      this.appState.dispatch(new StatsActions.AddCharts(response));
    });
  }
}

/*
 * Please review the https://github.com/AngularClass/angular2-examples/ repo for
 * more angular app examples that you may copy/paste
 * (The examples may not be updated as quickly. Please open an issue on github for us to update it)
 * For help or questions please contact us at @AngularClass on twitter
 * or our chat on Slack at https://AngularClass.com/slack-join
 */
