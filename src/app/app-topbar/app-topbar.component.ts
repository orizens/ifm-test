import { Component, OnInit } from '@angular/core';
import { AppState } from '../app.service';
import { IAppStore } from '../models';

import * as AppActions from '../store/app.reducer';

@Component({
  selector: 'app-topbar',
  styleUrls: ['./app-topbar.component.scss'],
  template: `
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <a class="navbar-brand" href="#"> {{ appName$ | async }} </a>

        <button class="btn btn-primary navbar-btn pull-right btn-menu visible-xs visible-sm"
          (click)="toggleMenu()">
          <i class="glyphicon glyphicon-menu-hamburger"></i>
        </button>
      </div>
      

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li class="active"><a href="#">Main <span class="sr-only">(current)</span></a></li>
          
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
  `
})

export class AppTopbarComponent {
  public appName$ = this.appState.select((store: IAppStore) => store.app.appName);

  constructor(private appState: AppState) {}

  public toggleMenu() {
    this.appState.dispatch(new AppActions.ToggleMenu());
  }
}
