import { TestBed, inject } from '@angular/core/testing';

import { AppTopbarComponent } from './app-topbar.component';
import { AppState } from '../app.service';
import * as AppActions from '../store/app.reducer';

describe('a app-topbar component', () => {
  let component: AppTopbarComponent;
  const appStateStub = jasmine.createSpyObj('appStateStub', [
    'dispatch'
  ]);
  appStateStub.select = () => 1;
  // register all needed dependencies
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AppTopbarComponent,
        { provide: AppState, useValue: appStateStub }
      ]
    });
  });

  // instantiation through framework injection
  beforeEach(inject([AppTopbarComponent], (AppTopbarComponent) => {
    component = AppTopbarComponent;
  }));

  it('should have an instance', () => {
    expect(component).toBeDefined();
  });

  it('should have a toggle menu feature', () => {
    expect(component.toggleMenu).toBeDefined();
  });

  it('should have an appName$ observable', () => {
    expect(component.appName$).toBeDefined();
  });

  it('should dipatch a toggle menu action', () => {
    const actual = appStateStub.dispatch;
    const expected = new AppActions.ToggleMenu();
    component.toggleMenu();
    expect(actual).toHaveBeenCalledWith(expected);
  });

});
