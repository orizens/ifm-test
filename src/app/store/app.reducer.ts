import { IAppSettings } from '../models';

export const ActionTypes = {
  MENU_TOGGLE: 'MENU_TOGGLE'
};

const initialState = {
  menuOpen: true,
  appName: 'ChartBoard'
};

export function reducer(state: IAppSettings = initialState, action): IAppSettings {
  switch (action.type) {
    case ActionTypes.MENU_TOGGLE: {
      return Object.assign({}, state, {
        menuOpen: !state.menuOpen
      });
    }

    default: {
      return state;
    }
  }
}

export class ToggleMenu {
  public type = ActionTypes.MENU_TOGGLE;
  // constructor() { }
}
