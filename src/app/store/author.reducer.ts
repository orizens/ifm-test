import { IUserProfile } from '../models';

export const ActionTypes = {
  UPDATE_NAME: 'UPDATE_NAME',
  UPDATE_WEBSITE: 'UPDATE_WEBSITE',
  UPDATE_IMAGE: 'UPDATE_IMAGE'
};

const initialState = {
  name: 'Oren Farhi',
  website: 'http://orizens.com'
};

export function reducer(state: IUserProfile = initialState, action): IUserProfile {
  switch (action.type) {
    case ActionTypes.UPDATE_NAME: {
      return Object.assign({}, state, {
        name: action.payload
      });
    }

    case ActionTypes.UPDATE_WEBSITE: {
      return Object.assign({}, state, {
        website: action.payload
      });
    }

    case ActionTypes.UPDATE_IMAGE: {
      return Object.assign({}, state, {
        image: action.payload
      });
    }

    default: {
      return state;
    }
  }
}
