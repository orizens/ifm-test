import {
  inject,
  async,
} from '@angular/core/testing';

import * as ChartsStore from './charts.reducer';
import { IChartsState } from '../models';

import * as mock from '../../assets/mock-data/mock-chart-data.json';

describe('The Player Search reducer', () => {
  const mockedState = (): IChartsState => ({
    sets: [],
    axisProp: ChartsStore.ChartProps.Axis.PROFITS,
    yaxisProp: ChartsStore.ChartProps.Yaxis.MONTH
  });

  it('should return current state when no valid actions have been made', () => {
    const state = mockedState();
    const actual = ChartsStore.reducer(state, { type: 'INVALID_ACTION' });
    const expected = state;
    expect(actual).toEqual(expected);
  });

  it('should ADD charts', () => {
    const state = mockedState();
    const action = new ChartsStore.AddCharts(mock);
    const actual = ChartsStore.reducer(state, action);
    const expected = [...state.sets, ...mock];
    expect(actual.sets.length).toBe(expected.length);
  });

  it('should update axis prop for display', () => {
    const state = mockedState();
    const action = new ChartsStore.UpdateAxisPropToShow(ChartsStore.ChartProps.Axis.INVENTORY);
    const actual = ChartsStore.reducer(state, action);
    const expected = ChartsStore.ChartProps.Axis.INVENTORY;
    expect(actual.axisProp).toBe(expected);
  });
});
