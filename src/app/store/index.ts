import { IAppStore } from '../models';
import * as App from './app.reducer';
import * as Author from './author.reducer';
import * as Charts from './charts.reducer';
import * as Stats from './stats.reducer';

export const StoreReducers = {
  app: App.reducer,
  charts: Charts.reducer,
  author: Author.reducer,
  stats: Stats.reducer
};
