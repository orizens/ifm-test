import { IChartsState, IChartItem, IChartSet } from '../models';

export const ActionTypes = {
  ADD_CHARTS: 'ADD_CHARTS',
  UPDATE_AXIS_PROP_TO_SHOW: 'UPDATE_AXIS_PROP_TO_SHOW',
  UPDATE_YAXIS_PROP_TO_SHOW: 'UPDATE_YAXIS_PROP_TO_SHOW'
};

export const ChartProps = {
  Axis: {
    INVENTORY: 'inventory',
    PROFITS: 'profits'
  },
  Yaxis: {
    MONTH: 'month'
  }
};

const initialState: IChartsState = {
  sets: [],
  axisProp: ChartProps.Axis.PROFITS,
  yaxisProp: ChartProps.Yaxis.MONTH
};

export function reducer(state: IChartsState = initialState, action): IChartsState {
  switch (action.type) {
    case ActionTypes.ADD_CHARTS: {
      return Object.assign({}, state , {
        sets: [...state.sets, ...action.payload]
      });
    }

    case ActionTypes.UPDATE_AXIS_PROP_TO_SHOW: {
      return Object.assign({}, state, {
        axisProp: action.payload
      });
    }
    default: {
      return state;
    }
  }
}

export class AddCharts {
  public type = ActionTypes.ADD_CHARTS;
  constructor(public payload: IChartSet[]) {}
}

export class UpdateAxisPropToShow {
  public type = ActionTypes.UPDATE_AXIS_PROP_TO_SHOW;
  constructor(public payload: string) {}
}

export class UpdateYaxisPropToShow {
  public type = ActionTypes.UPDATE_YAXIS_PROP_TO_SHOW;
  constructor(public payload: string) {}
}
