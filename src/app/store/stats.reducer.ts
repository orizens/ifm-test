import { IStatsChartsState, IStatsChartSet, IStatsChartItem } from '../models';

export const ActionTypes = {
  ADD_STATS_CHARTS: 'ADD_STATS_CHARTS',
  REPLACE_STATS_CHARTS: 'REPLACE_STATS_CHARTS',
};

export const ChartProps = {
  Axis: {
    INVENTORY: 'inventory',
    PROFITS: 'profits'
  },
  Yaxis: {
    MONTH: 'month'
  }
};

const initialState: IStatsChartsState = {
  sets: []
};

export function reducer(state: IStatsChartsState = initialState, action): IStatsChartsState {
  switch (action.type) {
    case ActionTypes.ADD_STATS_CHARTS: {
      return Object.assign({}, state , {
        sets: [...state.sets, ...action.payload]
      });
    }

    case ActionTypes.REPLACE_STATS_CHARTS: {
      return Object.assign({}, state, {
        sets: action.payload
      });
    }
    default: {
      return state;
    }
  }
}

export class AddCharts {
  public type = ActionTypes.ADD_STATS_CHARTS;
  constructor(public payload: IStatsChartSet) {}
}

export class ReplaceCharts {
  public type = ActionTypes.REPLACE_STATS_CHARTS;
  constructor(public payload: IStatsChartSet) {}
}
