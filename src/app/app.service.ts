import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/scan';

import { IAppStore } from './models';
import { StoreReducers } from './store';
import { Injectable } from '@angular/core';

const StoreActions = {
  STORE_INIT: 'STORE_INIT'
};

@Injectable()
export class AppState {

  // initial state for the app's store
  private _store: IAppStore = {};

  // a observable dispatcher which emits the latest
  // value for both new and previous subscribers
  private _dispatcher = new BehaviorSubject(this._store);

  // the internal _store is updated with every emitted value
  constructor() {
    this._dispatcher.subscribe((store) => this._store = store);
    this.dispatch({ type: StoreActions.STORE_INIT });
  }
  // already return a clone of the current state
  public get state() {
    // return this._state = this._clone(this._state);
    return this._dispatcher;
  }
  // never allow mutation
  public set state(value) {
    throw new Error('do not mutate the `.state` directly');
  }

  // allows to select a slice of the _store
  public select(selectorFunc: any) {
    return this.state
      .map(selectorFunc);
  }

  // Redux/ngrx-store "dispatch" implementation
  // changing the state of the store is performed
  // via dispatching actions only - no direct mutation of the store
  // all reducers are invoked with the action and
  // finally the result object is updated with the store
  // and emited with the dispatcher
  public dispatch(action: { type: string, payload?: any }) {
    const reducers = StoreReducers;
    const storeCopy = Object.assign({}, this._store);
    const nextStore = Object.keys(reducers).reduce((acc, curr) => {
      acc[curr] = reducers[curr](storeCopy[curr], action);
      return acc;
    }, storeCopy);
    this._dispatcher.next(nextStore);
  }
}
