import {
  Component,
  OnInit
} from '@angular/core';

import { AppState } from '../app.service';
import { IAppStore } from '../models';
import { StatsCenterService } from '../services/stats-center.service';
import * as StatsActions from '../store/stats.reducer';

@Component({
  selector: 'home',
  styleUrls: [ './home.component.scss' ],
  template: `
    <div class="panel panel-primary">
      <div class="panel-heading">The Statistics</div>
      <div class="panel-body">
        <section class="donuts">
          <donut-charts [data]="statsData$ | async"></donut-charts>
        </section>

        <section class="donuts-actions">
          <button class="btn btn-primary"
            (click)="load()">
            Load Random Statistics
          </button>
        </section>
      </div>
    </div>

    <div class="panel panel-primary">
      <div class="panel-heading">The Bars</div>
      <div class="panel-body">
        <section class="home-charts">
          <bar-charts class="col-md-12"
            [data]="chartData$ | async">
          </bar-charts>
        </section>
      </div>
    </div>
  `
})
export class HomeComponent {
  public chartData$ = this.appState.select((store: IAppStore) => store.charts);
  public statsData$ = this.appState.select((store: IAppStore) => store.stats);

  constructor(
    public appState: AppState,
    public statsCenterService: StatsCenterService
  ) {}

  public load() {
    this.statsCenterService.getRandomStats().subscribe((response) => {
      this.appState.dispatch(new StatsActions.ReplaceCharts(response));
    });
  }
}
