import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'about',
  styleUrls: [ './about.component.scss' ],
  template: `
		<iframe src="http://orizens.com/cv" border="0" width="100%" height="100%"></iframe>
	`
})

export class AboutComponent {

}
